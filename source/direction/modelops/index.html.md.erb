---
layout: secure_and_protect_direction
title: Product Stage Direction - ModelOps
description: "The ModelOps Stage focuses on extending GitLab with data science features and enabling customers to leverage data science workloads within GitLab."
canonical_path: "/direction/modelops/"
---

## On this page
{:.no_toc}

- TOC
{:toc}


<p align="center">
    <font size="+2">
        <b>Enable and empower data science workloads on GitLab</b>
    </font>
</p>

## Stage Overview

GitLab ModelOps aims to bring data science into Gitlab both within existing features to make them smarter and more intelligent, but also empowering Gitlab customers to [build and integrate data science workloads within GitLab](/solutions/data-science/).

 The ModelOps Stage is currently outside of the GitLab DevOps lifecycle. We believe that data science features can span across all DevOps stages, making existing features more intelligent and automated.

### Groups

There are two areas of relevance to GitLab ModelOps which we believe are critical to having end to end functioning data science workloads on GitLab:

* [MLOps](/handbook/engineering/incubation/mlops/) - Enabling customer data science use cases which include accessing and interacting with data, AI/ML toolchain integrations, and compute environment integrations.
* [DataOps](/direction/modelops/dataops) - Enabling data processing use cases like building, running & orchestrating Extract, Load, Transform (ELT) data pipelines to shape and process data for useful analysis.

With our learnings about building and deploying data science workloads with DataOps and MLOps, we will be putting that experience into practice with the stage's other groups:

* [Applied ML](/direction/modelops/applied_ml/) - Infusing data science into existing GitLab features.
* [Anti-Abuse](/direction/modelops/anti-abuse/) - Protecting GitLab instances against insider threat through anomolous activity detection and other data science techniques.

### Stage Direction Walkthrough & Commit Talk
Watch Sr. Director of Product David DeSanto, Engineering Manager Monmayuri Ray, and Principal Product Manager Taylor McCaslin discuss an overview of the Gitlab ModelOps stage, dig into the focus for each group, and answer common questions about this new area of investment for GitLab.

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WsxoiUB--uw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Commit Virtual 2021: What the ML is up with DevOps and AI? A ModelOps Overview*
In this session, we will discuss the three pillars of ModelOps, including how to integrate Data Science into DevOps. This will include a brief history on how we got here, as well as where we are going. We will discuss GitLab’s recent acquisition of UnReview and how GitLab plans to leverage ML/AI within our platform to improve user experience, as well as empower users to include ML/AI within their applications. We will conclude with some examples of how customers are empowering their data science workloads with GitLab today.
<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/7mUgGFgab4E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>


### Resourcing and Investment

GitLab ModelOps is currently composed of 4 groups with a variety of open roles we are actively recruiting:
* [MLOps](/handbook/engineering/incubation/mlops/) - An [experimental single-engineer groups](https://about.gitlab.com/company/team/structure/#single-engineer-groups)
* [Applied ML](/direction/modelops/applied-ml) - A [product group](/handbook/product/categories/#modelops-stage)
  * [Senior Backend Engineer Opening](https://boards.greenhouse.io/gitlab/jobs/5429916002)
* [Anti-abuse](/direction/modelops/anti-abuse) - A [product group](/handbook/product/categories/#modelops-stage)
  * [Sr. Backend Engineering (Anti-abuse) Opening](https://boards.greenhouse.io/gitlab/jobs/5360876002)
* [DataOps](/direction/modelops/dataops) - currently unstaffed, but a planned future product group


The ModelOps stage also receives support from several team members, from across different GitLab departments, dedicating approximately 20% of their time enabling data science workloads for existing GitLab Stages and Categories by enabling data science workloads. Our goal is to provide a blueprint to where GitLab should fund full teams to bring data science into GitLab features where relevant to empower customer's data science workloads. To learn more about GitLab’s investment areas, please visit the [Product Investments](https://about.gitlab.com/handbook/product/investment/#investment-types) section of the GitLab Handbook.

## The Plan

### What We Recently Completed

Today, the ModelOps Stage is actively staffing up. We've recently hired an Engineering Manager (Monmayuri Ray) and Product Manager (Taylor McCaslin) and are actively hiring multipule engineering roles (see above). With this said, there has been significant progress, including the following:

* Gitlab 13.9 - [Exposing GPU to GitLab Runner](https://docs.gitlab.com/runner/configuration/gpus.html) - [Video Demo](https://www.youtube.com/watch?v=x9nj-tksQDk)
* Internal Feature - Automatic Issue labeling model that is based on GitLab's internal issue tracking and label usage. This was our first small feature experiment to see if data science workloads could bring benefits to existing GitLab features. You can explore this working prototype in the Slack channel: #feed_tanuki-stan

### What We Are Currently Working On

* Exploring potential partners in this space to quickly integrate with vendors who have existing GitLab customers. [Propose a partner or integration here](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/24#note_711044721_.
* Actively developing data science enhancements to existing small GitLab feature sets to gain experience working with and deploying data science features on GitLab.
* Customer interviews as we explore the space to understand both customer personas and areas of potential product opportunities.
* Beginning to implement data science powered features into GitLab with [our recent acquisition of UnReview](https://about.gitlab.com/press/releases/2021-06-02-gitlab-acquires-unreview-machine-learning-capabilities.html) for smarter code reviewer suggestions.

### What's Next For Us

* [Integration of UnReview](https://gitlab.com/gitlab-org/gitlab/-/issues/340507) to power Recommended Code Reviewers
* [Improved Python Notebook experience](https://gitlab.com/groups/gitlab-org/-/epics/6589)
* [Potential idea list](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=group%3A%3Amachine+learning)

## Target audience

GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order:

* 🟩 - Targeted with strong support
* 🟨 - Targeted but incomplete support
* ⬜️ - Not targeted but might find value

### Today

To capitalize on the potential opportunities, the ModelOps Stage has features that make it useful to the following personas today:

1. 🟨 - Developers
1. 🟨 - Data scientists
1. 🟨 - Security Teams
1. ⬜️ - QA engineers / QA Teams

### Developers

Data Science workloads can be complicated and can leverage specialized hardware and development environments not common to traditional software development teams. The ModelOps stage is focused on the intersection of data scientists exploring models and feature development and the developers who must then deploy those data science features into production.

Personas
* [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
* [Delaney - Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)

### Data Scientists

Data scientists have unique roles within organizations. They are more scientists than developers, following hypotheses and data to explore models and develop data science-powered features.

We aim to serve data scientists as they balance art and science within software engineering teams. Data scientists wear a lot of hats to get from hypothesis to data science feature that generates value. GitLab is not a tool of choice for data scientists and we aim to change that by making it easy to configure, build, and execute data science feature development within GitLab.

Personas
* Daphne - Data Scientist - a new persona for GitLab we are actively exploring for use cases and workflows.

### Security Teams

The larger the organization, the harder it is for security teams to stay on top of everything happening in complex, ever-changing environments. As an organization's source code management and DevSecOps platform, GitLab holds a lot of sensitive, high-value data. We want to help security teams secure that data. This is a job to which automated data science features can be well suited, including monitoring high-value assets around the clock.

Personas
* [Sam - Security Analyst](/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
* [Alex - Security Operations Engineer](/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer)


<p align="center">
    <i><br>
    Last Reviewed: 2021-10-26<br>
    Last Updated: 2021-10-26
    </i>
</p>
